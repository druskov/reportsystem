﻿using ReportSystem.Domain.Enums;
using System.Collections.Generic;

namespace ReportSystem.Domain.Entities
{
    public class Employee : Entity
    {
        public Employee()
        {
            this.Tasks = new HashSet<TaskEmployee>(); // Initialize collections
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public EmployeeType Type { get; set; }

        public int DepartmentId { get; set; } // Foreign key

        public Department Department { get; set; } // Navigation property

        public HashSet<TaskEmployee> Tasks { get; set; } // Navigation property
    }
}
