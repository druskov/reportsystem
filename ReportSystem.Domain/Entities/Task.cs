﻿using System.Collections.Generic;

namespace ReportSystem.Domain.Entities
{
    public class Task : Entity
    {
        public Task()
        {
            this.Employees = new HashSet<TaskEmployee>();
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public int DepartmentId { get; set; } // Foreign key

        public Department Department { get; set; } // Navigation property

        public HashSet<TaskEmployee> Employees { get; set; } // Navigation property
    }
}
