﻿using System.Collections.Generic;

namespace ReportSystem.Domain.Entities
{
    public class Department : Entity
    {
        public Department()
        {
            this.Employees = new HashSet<Employee>();
        }

        public string Name { get; set; }

        public string Location { get; set; }

        public HashSet<Employee> Employees { get; set; } // Navigation property
    }
}
