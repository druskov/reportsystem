﻿namespace ReportSystem.Domain.Entities
{
    public class TaskEmployee
    {
        public int TaskEmployeeId { get; set; } // PrimaryKey

        public int TaskId { get; set; } // Foreign key
        public Task Task { get; set; } // Navigation property

        public int EmployeeId { get; set; } // Foreign key
        public Employee Employee { get; set; } // Navigation property

        public int? TimeToComplete { get; set; }
    }
}
