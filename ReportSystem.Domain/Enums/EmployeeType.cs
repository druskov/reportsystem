﻿namespace ReportSystem.Domain.Enums
{
    public enum EmployeeType
    {
        Boss,
        Manager,
        Programmer
    }
}
