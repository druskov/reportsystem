﻿using Microsoft.EntityFrameworkCore;
using ReportSystem.Domain.Entities;

namespace ReportSystem.Data
{
    public class ReportSystemDbContext : DbContext
    {
        public DbSet<Department> Departments { get; set; }

        public DbSet<Task> Tasks { get; set; }

        public DbSet<TaskEmployee> TasksEmployees { get; set; }

        public DbSet<Employee> Employees { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ReportSystemDbTest;Trusted_Connection=True;");
            }

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskEmployee>(entity =>
            {
                entity
                    .HasOne(te => te.Task)
                    .WithMany(te => te.Employees)
                    .HasForeignKey(te => te.TaskId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity
                    .HasOne(e => e.Department)
                    .WithMany(d => d.Employees)
                    .HasForeignKey(e => e.DepartmentId)
                    .OnDelete(DeleteBehavior.ClientCascade);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}