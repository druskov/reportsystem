﻿using MediatR;
using ReportSystem.Domain.Enums;

namespace ReportSystem.Application.Contracts.Domain.Commands
{
    public class CreateEmployeeCommand : IRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public EmployeeType Type { get; set; }

        public int DepartmentId { get; set; }
    }
}
