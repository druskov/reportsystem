﻿using Microsoft.EntityFrameworkCore;
using ReportSystem.Data;
using ReportSystem.Domain.Entities;
using ReportSystem.Domain.Enums;
using System;
using System.Linq;

namespace ReportSystem.Web
{
    class Program
    {
        static void Main()
        {
            using (var dbContext = new ReportSystemDbContext())
            {
                Console.WriteLine("Creating database...");
                dbContext.Database.EnsureDeleted();
                dbContext.Database.EnsureCreated();
                Console.WriteLine("Database is created successfully.");

                Console.WriteLine("Seeding database...");
                var newDepartment = new Department()
                {
                    Name = "SiT",
                    Location = "Shumen"
                };

                var employees = new Employee[]
                {
                    new Employee()
                    {
                        FirstName= "Dimitar",
                        LastName = "Ruskov",
                        PhoneNumber = "0894654898",
                        Type = EmployeeType.Programmer,
                        Department = newDepartment
                    },
                    new Employee()
                    {
                        FirstName= "Miroslav",
                        LastName = "Stoynov",
                        PhoneNumber = "0894654898",
                        Type = EmployeeType.Programmer,
                        Department = newDepartment
                    }
                };

                dbContext.Departments.Add(newDepartment);
                dbContext.Employees.AddRange(employees);

                dbContext.SaveChanges();

                Console.WriteLine("Database is ready.");

                Console.WriteLine("Press any key to delete data.");
                Console.ReadKey();

                var department = dbContext.Departments
                    .Include(d => d.Employees)
                    .FirstOrDefault(d => d.Id == 1);

                dbContext.Departments.Remove(department);

                dbContext.SaveChanges();
            }
        }
    }
}
