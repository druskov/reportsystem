﻿using MediatR;
using ReportSystem.Application.Contracts.Domain.Commands;
using ReportSystem.Data;
using ReportSystem.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ReportSystem.Application.Domain.Commands
{
    public class CreateEmployeeCommandHandler : IRequestHandler<CreateEmployeeCommand>
    {
        private readonly ReportSystemDbContext _reportSystemDbContext;

        public CreateEmployeeCommandHandler(ReportSystemDbContext reportSystemDbContext)
        {
            _reportSystemDbContext = reportSystemDbContext;
        }

        public async Task<Unit> Handle(CreateEmployeeCommand request, CancellationToken cancellationToken)
        {
            //var employee = new Employee()
            //{
            //    FirstName = request.FirstName,
            //    LastName = request.LastName,
            //    Type = request.Type,
            //    DepartmentId = request.DepartmentId,
            //};

            ////_reportSystemDbContext.Employees.Add(employee);
            //await _reportSystemDbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
